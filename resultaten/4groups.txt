Non-conflicting flow groups for flow 1. It has 14 groups in total.
    1, 2, 3, 4
    1, 2, 4, 7
    1, 2, 7, 8
    1, 3, 4, 10
    1, 3, 4, 14
    1, 4, 6, 7
    1, 4, 7, 10
    1, 4, 10, 11
    1, 4, 11, 14
    1, 6, 7, 12
    1, 7, 8, 10
    1, 7, 10, 12
    1, 10, 11, 12
    1, 11, 12, 14

Non-conflicting flow groups for flow 2. It has 6 groups in total.
    2, 1, 3, 4
    2, 1, 4, 7
    2, 1, 7, 8
    2, 3, 4, 15
    2, 4, 7, 15
    2, 7, 8, 15

Non-conflicting flow groups for flow 3. It has 6 groups in total.
    3, 1, 2, 4
    3, 1, 4, 10
    3, 1, 4, 14
    3, 2, 4, 15
    3, 4, 9, 10
    3, 4, 14, 15

Non-conflicting flow groups for flow 4. It has 18 groups in total.
    4, 1, 2, 3
    4, 1, 2, 7
    4, 1, 3, 10
    4, 1, 3, 14
    4, 1, 6, 7
    4, 1, 7, 10
    4, 1, 10, 11
    4, 1, 11, 14
    4, 2, 3, 15
    4, 2, 7, 15
    4, 3, 9, 10
    4, 3, 14, 15
    4, 5, 6, 7
    4, 5, 7, 10
    4, 5, 10, 11
    4, 5, 11, 14
    4, 6, 7, 15
    4, 7, 9, 10

Non-conflicting flow groups for flow 5. It has 8 groups in total.
    5, 4, 6, 7
    5, 4, 7, 10
    5, 4, 10, 11
    5, 4, 11, 14
    5, 6, 7, 13
    5, 7, 10, 13
    5, 10, 11, 13
    5, 11, 13, 14

Non-conflicting flow groups for flow 6. It has 6 groups in total.
    6, 1, 4, 7
    6, 1, 7, 12
    6, 4, 5, 7
    6, 4, 7, 15
    6, 5, 7, 13
    6, 7, 13, 15

Non-conflicting flow groups for flow 7. It has 18 groups in total.
    7, 1, 2, 4
    7, 1, 2, 8
    7, 1, 4, 6
    7, 1, 4, 10
    7, 1, 6, 12
    7, 1, 8, 10
    7, 1, 10, 12
    7, 2, 4, 15
    7, 2, 8, 15
    7, 4, 5, 6
    7, 4, 5, 10
    7, 4, 6, 15
    7, 4, 9, 10
    7, 5, 6, 13
    7, 5, 10, 13
    7, 6, 13, 15
    7, 8, 9, 10
    7, 9, 10, 13

Non-conflicting flow groups for flow 8. It has 4 groups in total.
    8, 1, 2, 7
    8, 1, 7, 10
    8, 2, 7, 15
    8, 7, 9, 10

Non-conflicting flow groups for flow 9. It has 4 groups in total.
    9, 3, 4, 10
    9, 4, 7, 10
    9, 7, 8, 10
    9, 7, 10, 13

Non-conflicting flow groups for flow 10. It has 14 groups in total.
    10, 1, 3, 4
    10, 1, 4, 7
    10, 1, 4, 11
    10, 1, 7, 8
    10, 1, 7, 12
    10, 1, 11, 12
    10, 3, 4, 9
    10, 4, 5, 7
    10, 4, 5, 11
    10, 4, 7, 9
    10, 5, 7, 13
    10, 5, 11, 13
    10, 7, 8, 9
    10, 7, 9, 13

Non-conflicting flow groups for flow 11. It has 8 groups in total.
    11, 1, 4, 10
    11, 1, 4, 14
    11, 1, 10, 12
    11, 1, 12, 14
    11, 4, 5, 10
    11, 4, 5, 14
    11, 5, 10, 13
    11, 5, 13, 14

Non-conflicting flow groups for flow 12. It has 4 groups in total.
    12, 1, 6, 7
    12, 1, 7, 10
    12, 1, 10, 11
    12, 1, 11, 14

Non-conflicting flow groups for flow 13. It has 6 groups in total.
    13, 5, 6, 7
    13, 5, 7, 10
    13, 5, 10, 11
    13, 5, 11, 14
    13, 6, 7, 15
    13, 7, 9, 10

Non-conflicting flow groups for flow 14. It has 6 groups in total.
    14, 1, 3, 4
    14, 1, 4, 11
    14, 1, 11, 12
    14, 3, 4, 15
    14, 4, 5, 11
    14, 5, 11, 13

Non-conflicting flow groups for flow 15. It has 6 groups in total.
    15, 2, 3, 4
    15, 2, 4, 7
    15, 2, 7, 8
    15, 3, 4, 14
    15, 4, 6, 7
    15, 6, 7, 13