package verkeersregeling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Main {
	public static Map<Integer, List<int[]>> flow4Groups = new LinkedHashMap<Integer, List<int[]>>();
	public static Set<RegelStructuur> possibleStructs = new HashSet<RegelStructuur>();
	public static Map<RegelStructuur, Double> structCyliAmount = new HashMap<RegelStructuur, Double>();
	
	public static void main(String[] args) {
		Inputs.testSymetry();
		
		find4Groups();
		
		//print4Groep();
		
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(1,5,9,15));
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(2,5,9,12));
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(2,6,9,11));
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(2,6,10,14));
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(3,5,8,12));
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(3,6,8,11));
		//printConflictGroupRegelStructs(getRegelStructForConflictGroup(3,8,12,13));
		
		findRegelStructs();
		calcCyclusAmount();
		
		//printAllStructResults();
		printBestStructs();
	}
	
 	public static void find4Groups() {
 		for(int i = 1; i <= 15; i++) {
			List<int[]> largestGroups = new ArrayList<int[]>();
			
			for(int a = 1; a <= 15; a++) {
				if(a == i) continue;
				
				for(int b = 1; b <= 15; b++) {
					if(b == i || b <= a) continue;
					
					for(int c = 1; c <= 15; c++) {
						if(c == i || c <= b) continue;
						
						if(!conflicts(i, a, b, c)) largestGroups.add(new int[] {i, a, b, c});
					}
				}
			}
			
			flow4Groups.put(i, largestGroups);
 		}
	}
 	
	public static boolean conflicts(int... ints) {
		for(int index = 0; index < ints.length; index++) {
			for(int indexC = 0; indexC < ints.length; indexC++) {
				if(index == indexC || index > indexC) continue;
				
				if(Inputs.conflicts(ints[index], ints[indexC])) return true;
			}
		}
		
		return false;
	}
	
	public static void findRegelStructs() {
		possibleStructs.addAll(getRegelStructForConflictGroup(1,5,9,15));
		possibleStructs.addAll(getRegelStructForConflictGroup(2,5,9,12));
		possibleStructs.addAll(getRegelStructForConflictGroup(2,6,9,11));
		possibleStructs.addAll(getRegelStructForConflictGroup(2,6,10,14));
		possibleStructs.addAll(getRegelStructForConflictGroup(3,5,8,12));
		possibleStructs.addAll(getRegelStructForConflictGroup(3,6,8,11));
		possibleStructs.addAll(getRegelStructForConflictGroup(3,8,12,13));
		
		List<RegelStructuur> tempPermutationStructs = new ArrayList<RegelStructuur>();
		for(RegelStructuur struct : possibleStructs) {
			for(RegelStructuur structPermutation : struct.getPermutations()) {
				if(struct == structPermutation) continue;
				tempPermutationStructs.add(structPermutation);
			}
		}
		
		possibleStructs.addAll(tempPermutationStructs);
	}
	
	public static List<RegelStructuur> getRegelStructForConflictGroup(int a, int b, int c, int d) {
		List<RegelStructuur> validGroups = new ArrayList<RegelStructuur>();
		
		for(int[] arrA : flow4Groups.get(a)) {
			for(int[] arrB : flow4Groups.get(b)) {
				for(int[] arrC : flow4Groups.get(c)) {
					for(int[] arrD : flow4Groups.get(d)) {
						if(Util.containsAllFlows(arrA, arrB, arrC, arrD)) validGroups.add(new RegelStructuur(new int[][] {arrA, arrB, arrC, arrD}));
						
					}
				}
			}
		}
		
		return validGroups;
	}
	
	public static void calcCyclusAmount() {
		for(RegelStructuur struct : possibleStructs) {
			double leftOverTime = 3600 - struct.getTijdVoorVerkeersStroom();
			double cycliAmount = leftOverTime / struct.verliesTijd();
			
			structCyliAmount.put(struct, cycliAmount);
		}
	}
 	
	public static double tijdVoorStroomVerkeersVraag(int stroom) {
		return Inputs.getIntensiteit(stroom) / ((Inputs.getBannen(stroom) * Inputs.maxIntensitietPerBaan) / 3600.0);
	}
	
	public static void print4Groep() {
		for(Entry<Integer, List<int[]>> entry : flow4Groups.entrySet()) {
			System.out.println("Non-conflicting flow groups for flow " + entry.getKey() + ". It has " + entry.getValue().size() + " groups in total.");
			for(int[] group : entry.getValue()) {
				String flows = "";
				for(int flow : group) {
					if(!flows.isEmpty()) flows += ", ";
					flows += flow;
				}
				System.out.println("    " + flows);
			}
			System.out.println();
		}
	}
	
	public static void printConflictGroupRegelStructs(List<RegelStructuur> structs) {
		System.out.println("Amount of options for conflict group " + structs.get(0).conflictGroup + " is " + structs.size());
		int i = 1;
		for(RegelStructuur struct : structs) {
			System.out.println("Option " + i + " with a verkeers stroom time loss van " + struct.getTijdVoorVerkeersStroom() + " s.");
			struct.printFlowOrde();
			System.out.println();
			i++;
		}
		System.out.println();
	}
	
	public static void printAllStructResults() {
		List<String> output = new ArrayList<String>();
		
		for(Entry<RegelStructuur, Double> e : structCyliAmount.entrySet()) {
			output.add(e.getKey() + " - " + e.getValue());
		}
		
		Collections.sort(output);
		
		for(String s : output) System.out.println(s);
	}
	
	public static void printBestStructs() {
		List<RegelStructuur> bestStructs = new ArrayList<RegelStructuur>();
		double bestCycliAmount = 0;
		
		for(Entry<RegelStructuur, Double> e : structCyliAmount.entrySet()) {
			if(e.getValue() >= bestCycliAmount) {
				if(e.getValue() > bestCycliAmount) bestStructs.clear();
				bestCycliAmount = e.getValue();
				bestStructs.add(e.getKey());
			}
		}
		
		for(RegelStructuur struct : bestStructs) {
			System.out.println(struct + " - " + bestCycliAmount);
			struct.printFlowOrde();
			System.out.println();
		}
	}
}
