package verkeersregeling;

public class Inputs {
	public static final boolean[][] conflictMatrix = {
		{false, false, false, false, true, false, false, false, true, false, false, false, true, false, true},
		{false, false, false, false, true, true, false, false, true, true, true, true, true, true, false}, 
		{false, false, false, false, true, true, true, true, false, false, true, true, true, false, false}, 
		{false, false, false, false, false, false, false, true, false, false, false, true, true, false, false}, 
		{true, true, true, false, false, false, false, true, true, false, false, true, false, false, true}, 
		{false, true, true, false, false, false, false, true, true, true, true, false, false, true, false}, 
		{false, false, true, false, false, false, false, false, false, false, true, false, false, true, false}, 
		{false, false, true, true, true, true, false, false, false, false, true, true, true, true, false}, 
		{true, true, false, false, true, true, false, false, false, false, true, true, false, true, true}, 
		{false, true, false, false, false, true, false, false, false, false, false, false, false, true, true}, 
		{false, true, true, false, false, true, true, true, true, false, false, false, false, false, true}, 
		{false, true, true, true, true, false, false, true, true, false, false, false, true, false, true}, 
		{true, true, true, true, false, false, false, true, false, false, false, true, false, false, false}, 
		{false, true, false, false, false, true, true, true, true, true, false, false, false, false, false}, 
		{true, false, false, false, true, false, false, false, true, true, true, true, false, false, false}
	};
	
	public static final int[][] ontruimingstijdMatrix = {
		{-1, -1, -1, -1, 1, -1, -1, -1, 1, -1, -1, -1, 1, -1, 2},
		{-1, -1, -1, -1, 1, 2, -1, -1, 2, 2, 2, 2, 1, 4, -1},
		{-1, -1, -1, -1, 1, 2, 3, 3, -1, -1, 2, 2, 1, -1, -1},
		{-1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, 1, 2, -1, -1},
		{3, 3, 2, -1, -1, -1, -1, 1, 2, -1, -1, 1, -1, -1, 4},
		{-1, 2, 2, -1, -1, -1, -1, 1, 2, 3, 3, -1, -1, 3, -1},
		{-1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1},
		{-1, -1, 1, 3, 3, 3, -1, -1, -1, -1, 1, 1, 4, 1, -1},
		{3, 3, -1, -1, 2, 2, -1, -1, -1, -1, 1, 1, -1, 1, 4},
		{-1, 2, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, 3, 1},
		{-1, 1, 1, -1, -1, 1, 3, 3, 3, -1, -1, -1, -1, -1, 1},
		{-1, 2, 2, 3, 3, -1, -1, 2, 2, -1, -1, -1, 4, -1, 1},
		{6, 6, 6, 6, -1, -1, -1, 4, -1, -1, -1, 5, -1, -1, -1},
		{-1, 5, -1, -1, -1, 5, 6, 6, 6, 6, -1, -1, -1, -1, -1},
		{6, -1, -1, -1, 5, -1, -1, -1, 5, 6, 6, 6, -1, -1, -1}
	};
	
	public static final int[] intensiteit = {454, 344, 265, 193, 278, 211, 236, 279, 403, 398, 307, 364, -1, -1, -1};
	public static final double[] bannen = {1, 3, 1, 1, 2, 2, 0.5, 2.5, 2, 2, 2, 2, -1, -1, -1};
	public static final int maxIntensitietPerBaan = 1800;
	public static final int geelPlusStartTijd = 4;
	
	public static int matrixIndexToArrayIndex(int i) {
		return i - 1;
	}
	
	public static boolean conflicts(int x, int y) {
		if(x == y) throw new IllegalArgumentException("Get same x-y " + x);
		return conflictMatrix[matrixIndexToArrayIndex(y)][matrixIndexToArrayIndex(x)];
	}
	
	public static int getOntruimingstijd(int from, int to) {
		int i = ontruimingstijdMatrix[matrixIndexToArrayIndex(from)][matrixIndexToArrayIndex(to)];
		if(i == -1) throw new IllegalArgumentException("Ontruimingstijd is -1 for (form-to) " + from + "-" + to);
		return i;
	}
	
	public static int getIntensiteit(int flow) {
		if(flow > 12) throw new IllegalArgumentException("No intensiteit voor flows hoger dan 12.");
		return intensiteit[matrixIndexToArrayIndex(flow)];
	}
	
	public static double getBannen(int flow) {
		if(flow > 12) throw new IllegalArgumentException("No bannen voor flows hoger dan 12.");
		return bannen[matrixIndexToArrayIndex(flow)];
	}
	
	public static void testSymetry() {
		for(int x = 1; x <= 15; x++) {
			for(int y = 1; y <= 15; y++) {
				int t1 = ontruimingstijdMatrix[matrixIndexToArrayIndex(x)][matrixIndexToArrayIndex(y)];
				int t2 = ontruimingstijdMatrix[matrixIndexToArrayIndex(y)][matrixIndexToArrayIndex(x)];
				if((t1 != -1 && t2 == -1) || (t1 == -1 && t2 != -1)) System.out.println("Missing symetry in ontruimingstijdMatrix at: " + x + "-" + y);
				
				if(x == y) continue;
				if(conflicts(x,y) != conflicts(y,x)) System.out.println("Missing symetry in ontruimingstijdMatrix at: " + x + "-" + y);
			}
		}
	}
}
