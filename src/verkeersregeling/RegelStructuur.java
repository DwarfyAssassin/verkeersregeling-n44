package verkeersregeling;

import java.util.ArrayList;
import java.util.List;

public class RegelStructuur {
	public int[][] flowOrder;
	public int permutation;
	public RegelStructuur[] permutations;
	public String conflictGroup;
	
	public RegelStructuur(int[][] flowOrde) {
		this(flowOrde, 1);
		conflictGroup = "" + flowOrde[0][0] + "-" + flowOrde[1][0] + "-" + flowOrde[2][0] + "-" + flowOrde[3][0];
	}
	
	public RegelStructuur(int[][] flowOrde, int permutation) {
		this.flowOrder = flowOrde;
		this.permutation = permutation;
	}
	
	public RegelStructuur[] getPermutations() {
		if(permutations != null) return permutations;
		
		RegelStructuur[] permutations = new RegelStructuur[6];
		permutations[0] = this;
		permutations[1] = new RegelStructuur(new int[][] {flowOrder[0], flowOrder[1], flowOrder[3], flowOrder[2]}, 2);
		permutations[2] = new RegelStructuur(new int[][] {flowOrder[0], flowOrder[2], flowOrder[1], flowOrder[3]}, 3);
		permutations[3] = new RegelStructuur(new int[][] {flowOrder[0], flowOrder[2], flowOrder[3], flowOrder[1]}, 4);
		permutations[4] = new RegelStructuur(new int[][] {flowOrder[0], flowOrder[3], flowOrder[1], flowOrder[2]}, 5);
		permutations[5] = new RegelStructuur(new int[][] {flowOrder[0], flowOrder[3], flowOrder[2], flowOrder[1]}, 6);
		permutations[1].conflictGroup = this.conflictGroup;
		permutations[2].conflictGroup = this.conflictGroup;
		permutations[3].conflictGroup = this.conflictGroup;
		permutations[4].conflictGroup = this.conflictGroup;
		permutations[5].conflictGroup = this.conflictGroup;
		this.permutations = permutations;
		permutations[1].permutations = permutations;
		permutations[2].permutations = permutations;
		permutations[3].permutations = permutations;
		permutations[4].permutations = permutations;
		permutations[5].permutations = permutations;
		
		return permutations;
	}
	
	public int getTijdVoorVerkeersStroom() {
		int totalTimeLoss = 0;
		for(int[] group : flowOrder) {
			
			if(group[0] <= 12) {
				totalTimeLoss += Main.tijdVoorStroomVerkeersVraag(group[0]);
			}
			else {
				double timeLossGroup = 0;
				
				for(int stroom : group) {
					if(stroom > 12) continue;
					
					double timeLossFlow = Main.tijdVoorStroomVerkeersVraag(stroom);
					if(timeLossFlow > timeLossGroup) timeLossGroup = timeLossFlow;
				}
				
				totalTimeLoss += timeLossGroup;
			}
		}
		
		return totalTimeLoss;
		
//		int duplicateFlow = findDuplicateFlow();
//		
//		int totalTimeLoss = 0;
//		for(int[] group : flowOrder) {
//			double timeLossGroup = 0;
//			for(int stroom : group) {
//				if(stroom > 12 || duplicateFlow == stroom) continue;
//				
//				double timeLossFlow = Main.tijdVoorStroomVerkeersVraag(stroom);
//				if(timeLossFlow > timeLossGroup) timeLossGroup = timeLossFlow;
//			}
//			
//			totalTimeLoss += timeLossGroup;
//		}
//		
//		return totalTimeLoss;
	}
	
	public int verliesTijd() {
		int totalTimeLoss = 4*Inputs.geelPlusStartTijd;
		
		totalTimeLoss += Inputs.getOntruimingstijd(flowOrder[0][0], flowOrder[1][0]);
		totalTimeLoss += Inputs.getOntruimingstijd(flowOrder[1][0], flowOrder[2][0]);
		totalTimeLoss += Inputs.getOntruimingstijd(flowOrder[2][0], flowOrder[3][0]);
		totalTimeLoss += Inputs.getOntruimingstijd(flowOrder[3][0], flowOrder[0][0]);
		
		return totalTimeLoss;
	}
	
	public int findDuplicateFlow() {
		List<Integer> foundFlows = new ArrayList<Integer>();
		for(int[] group : flowOrder) {
			for(int stroom : group) {
				if(foundFlows.contains(stroom)) return stroom;
				foundFlows.add(stroom);
			}
		}
		
		return -1;
	}
	
	public String toString() {
        return conflictGroup + " (" + permutation + ")";
    }
	
	public void printFlowOrde() {
		System.out.println("    " + flowOrder[0][0] + (flowOrder[0][0] > 9 ? " " : "  ") + flowOrder[0][1] + (flowOrder[0][1] > 9 ? " " : "  ") + flowOrder[0][2] + (flowOrder[0][2] > 9 ? " " : "  ") + flowOrder[0][3]);
		System.out.println("    " + flowOrder[1][0] + (flowOrder[1][0] > 9 ? " " : "  ") + flowOrder[1][1] + (flowOrder[1][1] > 9 ? " " : "  ") + flowOrder[1][2] + (flowOrder[1][2] > 9 ? " " : "  ") + flowOrder[1][3]);
		System.out.println("    " + flowOrder[2][0] + (flowOrder[2][0] > 9 ? " " : "  ") + flowOrder[2][1] + (flowOrder[2][1] > 9 ? " " : "  ") + flowOrder[2][2] + (flowOrder[2][2] > 9 ? " " : "  ") + flowOrder[2][3]);
		System.out.println("    " + flowOrder[3][0] + (flowOrder[3][0] > 9 ? " " : "  ") + flowOrder[3][1] + (flowOrder[3][1] > 9 ? " " : "  ") + flowOrder[3][2] + (flowOrder[3][2] > 9 ? " " : "  ") + flowOrder[3][3]);
	}
}
