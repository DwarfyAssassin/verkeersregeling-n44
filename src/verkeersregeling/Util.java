package verkeersregeling;

public class Util {
	public static boolean containsAllFlows(int[]... arrs) {
		for(int index = 1; index <= 15; index++) {
			if(!containsNumber(index, arrs)) return false;
		}
		
		return true;
	}
	
	public static boolean containsNumber(int find, int[]... arrs) {
		for(int[] arr : arrs) {
			for(int i : arr) {
				if(i == find) return true;
			}
		}
		
		return false;
	}
}
